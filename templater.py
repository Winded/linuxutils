from django.conf import settings
settings.configure()

import os
from django.template import Template, Context

def make(template, context):

	if type(template) != Template:
		template = Template(template)

	if type(context) != Context:
		context = Context(context)

	response = template.render(context)
	return response

def make_multiple(template, ctx_array):

	if type(template) != Template:
		template = Template(template)

	response = []

	for ctx in ctx_array:
		r = make(template, ctx)
		response.append(r)

	return response
from sys import argv
import os
import crypt

def make_users(delete = False, prefix = "user", count = 1, bashrc_src = ""):

	action = "Creating"
	if delete:
		action = "Deleting"
	print("%s %i users with prefix %s" % (action, count, prefix))

	for i in range(1, count + 1):

		uname = "%s%i" % (prefix, i)

		print("%s user %s" % (action, uname))

		if not delete:
			pwd = crypt.crypt(uname, "22")
			resp = os.system("useradd -m -p %s %s" % (pwd, uname))
			print(resp)
			home = os.path.join('/home', uname)
			www = os.path.join(home, "www")
			bashrc = os.path.join(home, ".bashrc")

			os.system("cp %s %s" % (bashrc_src, bashrc))
			os.system("mkdir %s" % www)
			os.system("chown -R %s %s" % (uname, home))
			os.system("chmod -R 755 %s" % home)

		else:
			resp = os.system("userdel -r %s" % uname)
			print(resp)

	print("User generation finished.")

def query_args():

	args = {}

	delete = raw_input("Create or delete? (C/d): ")
	if delete.lower() == "d":
		delete = True
	else:
		delete = False
	args["delete"] = delete

	prefix = raw_input("Enter prefix (blank for 'user'): ")
	if not prefix:
		prefix = "user"

	count = raw_input("Enter user count: ")
	try:
		count = int(count)
	except:
		print("Count wasn't a valid number. fuk u")
		return None

	args["prefix"] = prefix
	args["count"] = count
	return args

if __name__ == "__main__":
	args = query_args()
	if args:
		make_users(**args)

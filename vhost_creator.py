from sys import argv
import os
from django.template import Template, Context

from templater import make as tmake

class Vhost(object):
	def __init__(self, user, ip_end):
		self.user = user
		self.ip_end = ip_end

	user = ""
	ip_end = ""

def make_vhosts(path, template, prefix = "user", count = 1, ip_start = 0):

	print("Creating vhost files for %i users with prefix %s" % (count, prefix))

	tempf = file(template, "r")
	temp = tempf.read()

	vhosts = []

	for i in range(1, count + 1):

		uname = "%s%i" % (prefix, i)
		ip = ip_start + i

		print("Creating vhost for %s" % uname)

		vhost = Vhost(uname, ip)
		vhosts.append(vhost)

	ctx = { "vhosts": vhosts }

	result = tmake(temp, ctx)
	f = file(os.path.join(path, "vhosts"), "wb")
	f.write(result)
	f.flush()

	print("Vhost files succesfully generated, yayz!")

def query_args():

	path = raw_input("Enter path (blank for cwd): ")
	if not path:
		path = os.getcwd()
	else:
		path = os.path.abspath(path)

	template = raw_input("Enter template path: ")
	if not template:
		print("fak u")
		return None
	template = os.path.abspath(template)

	prefix = raw_input("Enter prefix (blank for 'user'): ")
	if not prefix:
		prefix = "user"

	count = raw_input("Enter user count: ")
	try:
		count = int(count)
	except:
		print("Count wasn't a valid number. fuk u")
		return None

	ip_start = raw_input("Enter IP end start point (blank for 0): ")
	if not ip_start:
		ip_start = 0
	else:
		ip_start = int(ip_start)

	return [path, template, prefix, count, ip_start]

if __name__ == "__main__":
	args = query_args()
	if args:
		make_vhosts(*args)

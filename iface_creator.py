import os

from templater import make as tmake

class Interface(object):
	id = None
	ip_end = None

	def __init__(self, id, ip_end):
		self.id = id
		self.ip_end = ip_end

def make_interfaces(template, target, physical = "eth0", base = 0, ip_base = 0, count = 1):

	temp = file(template, "r").read()

	interfaces = []

	for i in range(1, count + 1):
		v = Interface(base + i, ip_base + i)
		interfaces.append(v)

	ctx = {
		"physical": physical,
		"interfaces": interfaces
	}

	res = tmake(temp, ctx)

	t = file(target, "wb")
	t.write(res)
	t.flush()

	print("Done!")

def query_args():

	r = {}

	template = raw_input("Enter template path: ")
	if not template:
		print("fak u")
		return None
	r["template"] = os.path.abspath(template)

	target = raw_input("Enter target path: ")
	if not target:
		print("fak u")
		return None
	r["target"] = os.path.abspath(target)

	physical = raw_input("Enter physical if (blank for eth0): ")
	if not physical:
		physical = "eth0"
	r["physical"] = physical

	base = raw_input("Enter base ID (blank for 0): ")
	if not base:
		base = 0
	r["base"] = int(base)

	ip_base = raw_input("Enter IP base (blank for 0): ")
	if not ip_base:
		ip_base = 0
	r["ip_base"] = int(ip_base)

	count = raw_input("Enter count: ")
	if not count:
		print("fak u")
		return None
	r["count"] = int(count)

	return r

if __name__ == "__main__":
	args = query_args()
	if args:
		make_interfaces(**args)